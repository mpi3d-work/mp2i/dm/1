(* 1. *)
let rec avant_dernier = function
  | [ x; _ ] -> x
  | _ :: l -> avant_dernier l
  | [] -> failwith "Liste trop courte"

(* 2. *)
let rec n_ieme n = function
  | [] -> None
  | x :: l -> if n > 1 then n_ieme (n - 1) l else Some x

(* 3. *)
(* Comparaison en entier. On ne peut pas utiliser a - b car pas polymorphe *)
let cmp a b = if a < b then -1 else if a > b then 1 else 0

let rec signe s = function
  | [] | [ _ ] -> true
  | x :: y :: l ->
      let d = cmp x y in
      s * d >= 0
      && signe
           (if s = 0 then (* Monotonie pas encore determiné *)
              d else s)
           (y :: l)

let monotone = signe 0 (* Application partielle *)

(* 4. *)
let rec _somme_cumulee n = function
  | [] -> []
  | x :: l ->
      let n = n + x in
      n :: _somme_cumulee n l

let somme_cumulee = _somme_cumulee 0 (* Application partielle *)

(* 5. *)
let rec map_map p = function
  | [] -> ([], [])
  | x :: l ->
      let l1, l2 = map_map p l in
      if p x then (x :: l1, l2) else (l1, x :: l2)

(* 6. *)
(* La flemme de faire 2 fonctions *)
let rec produit l1 l2 =
  match l1 with
  | [] -> []
  | [ x ] -> ( match l2 with [] -> [] | y :: l2 -> (x, y) :: produit [ x ] l2)
  | x :: l1 -> produit [ x ] l2 @ produit l1 l2

(* 7. *)
let rec listes = function
  | [] -> []
  | [] :: l -> listes l
  | (x :: y) :: l -> x :: listes (y :: l)

(* 8. *)
(* Fonction qui compte le nombre de repetition *)
let rec compte e = function
  | [] -> (0, [])
  | x :: l ->
      if x = e then
        let c, l = compte e l in
        (c + 1, l)
      else (0, x :: l)

let rec encode = function
  | [] -> []
  | x :: l ->
      let c, l = compte x l in
      (x, c + 1) :: encode l

let rec decode = function
  | [] -> []
  | (_, 0) :: l -> decode l
  | (x, c) :: l -> x :: decode ((x, c - 1) :: l)
