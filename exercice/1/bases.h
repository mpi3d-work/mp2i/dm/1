#ifndef BASES_H
#define BASES_H

// 1.
int operation(char op, int a, int b);

// 2.
unsigned int pgcd(unsigned int a, unsigned int b);

// 3.
unsigned int n_chiffres(unsigned int n);

// 4.
bool triplet_p(unsigned int a, unsigned int b, unsigned int c);

// 5.
unsigned int n_triplet_p(unsigned int n);

#endif
