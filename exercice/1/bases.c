#include <stdio.h>
#include <assert.h>
#include <stdbool.h>

// 1.
int operation(char op, int a, int b)
{
    // On s'assure que a et b ne sont pas nuls, d'après la consigne
    assert(a != 0 && b != 0);
    // On s'assure que op est un opérateur valide. Pas besoin d'optimiser, le compilateur le fera pour nous.
    assert(op == '+' || op == '-' || op == '*' || op == '/' || op == '%');

    switch (op)
    {
    case '+':
        return a + b;
    case '-':
        return a - b;
    case '*':
        return a * b;
    case '/':
        // b != 0, donc a / b est défini
        return a / b;
    case '%':
        // b != 0, donc a % b est défini
        return a % b;
    }

    // On ne devrait jamais arriver ici
    return 0; // On retourne 0 pour éviter un warning du compilateur
}

// 1. / 6.
unsigned int test_operation()
{
    // On remarquera que l'on n'utilise pas de #define pour remplacer le 5. Ça aurait été bien utile mais...
    // Entrée: opérateur, a, b
    char ops[5] = {'+', '-', '*', '/', '%'};
    int as[5] = {1, 2, 3, 4, 5};
    int bs[5] = {1, 2, 3, 4, 5};

    // Sortie: résultat
    int ress[5] = {2, 0, 9, 1, 0};

    unsigned int cnt = 0; // Compteur de tests ratés

    // Juste des declarations de variables, rien de spécial
    char *neg, *pas, op;
    int a, b, res;

    for (unsigned int inc = 0; inc < 5; inc += 1) // On souligne ici l'utilisation du += pour incrémenter inc de 1
    {
        op = ops[inc];
        a = as[inc];
        b = bs[inc];

        res = operation(op, a, b); // Calcul du résultat

        if (res == ress[inc]) // On verifie que le résultat est le bon
            neg = pas = "";   // On ne nie pas la phrase
        else
        {
            cnt += 1; // On rajoute 1 au compteur de tests ratés

            // On nie la phrase
            neg = "n'";
            pas = " pas";
        }
        printf("La fonction operation appelée avec les paramètres %c, %d, %d renvoie %d ce qui %sest%s le bon résultat.\n", op, a, b, res, neg, pas);
    }

    return cnt; // Compteur de tests ratés. Surement 0 mais on ne sait jamais.
}

// 2.
unsigned int pgcd(unsigned int a, unsigned int b) // Ici, pgcd(0, 0) == 0. C'est un choix arbitraire.
{
    unsigned int tmp; // Petite variable temporaire...

    while (b != 0)
    {
        tmp = b;
        b = a % b; // On peut utiliser l'opérateur % car b != 0
        a = tmp;
    }

    return a; // Bon rien d'extraordinaire. Juste un pgcd quoi.
}

// 2. / 6.
unsigned int test_pgcd() // Pur copier-coller de test_operation() donc pas de commentaire
{
    unsigned int as[5] = {0, 1, 2, 3, 4};
    unsigned int bs[5] = {4, 3, 2, 1, 0};
    unsigned int ress[5] = {4, 1, 2, 1, 4};

    unsigned int cnt = 0;
    char *neg, *pas;
    unsigned int a, b, res;
    for (unsigned int inc = 0; inc < 5; inc += 1)
    {
        a = as[inc];
        b = bs[inc];

        res = pgcd(a, b);

        if (res == ress[inc])
            neg = pas = "";
        else
        {
            cnt += 1;
            neg = "n'";
            pas = " pas";
        }
        printf("La fonction pgcd appelée avec les paramètres %u, %u renvoie %u ce qui %sest%s le bon résultat.\n", a, b, res, neg, pas);
    }

    return cnt;
}

// 3.
unsigned int n_chiffres(unsigned int n)
{
    if (n == 0)
        return 1; // 0 a 1 chiffre

    unsigned int cnt = 0; // Encore un compteur

    while (n != 0)
    {
        n /= 10;  // On divise par 10 pour enlever le dernier chiffre
        cnt += 1; // On incrémente le compteur
        // On ne fait rien d'autre
    }

    return cnt;
}

// 3. / 6.
unsigned int test_n_chiffres() // Encore un `cp`. C'est vraiment dommage que l'on n'ai pas le droit au macros...
{
    unsigned int ns[5] = {0, 5, 10, 50, 100};
    unsigned int ress[5] = {1, 1, 2, 2, 3};

    unsigned int cnt = 0;
    char *neg, *pas;
    unsigned int n, res;
    for (unsigned int inc = 0; inc < 5; inc += 1)
    {
        n = ns[inc];

        res = n_chiffres(n);

        if (res == ress[inc])
            neg = pas = "";
        else
        {
            cnt += 1;
            neg = "n'";
            pas = " pas";
        }
        printf("La fonction n_chiffres appelée avec le paramètre %u renvoie %u ce qui %sest%s le bon résultat.\n", n, res, neg, pas);
    }

    return cnt;
}

// 4.
bool triplet_p(unsigned int a, unsigned int b, unsigned int c)
{
    if (a == 0 || b == 0 || c == 0 || a >= b || b >= c)
        return false; // On ne peut pas avoir de triplet pythagoricien avec un 0 ni dans le désordre

    return a * a + b * b == c * c; // On renvoie le résultat du test
}

// 4. / 6.
unsigned int test_triplet_p()
{
    unsigned int as[5] = {0, 3, 4, 5, 6};
    unsigned int bs[5] = {0, 4, 3, 6, 5};
    unsigned int cs[5] = {0, 5, 6, 3, 4};
    bool ress[5] = {false, true, false, false, false};

    unsigned int cnt = 0;
    char *neg, *pas;
    unsigned int a, b, c;
    bool res;
    char *sres; // Variable pour afficher les booléens
    for (unsigned int inc = 0; inc < 5; inc += 1)
    {
        a = as[inc];
        b = bs[inc];
        c = cs[inc];

        res = triplet_p(a, b, c);

        if (res == ress[inc])
            neg = pas = "";
        else
        {
            cnt += 1;
            neg = "n'";
            pas = " pas";
        }

        // On trenforme le booléen en chaîne de caractères
        if (res)
            sres = "true";
        else
            sres = "false";

        printf("La fonction triplet_p appelée avec les paramètres %u, %u, %u renvoie %s ce qui %sest%s le bon résultat.\n", a, b, c, sres, neg, pas);
    }

    return cnt;
}

// 5.
unsigned int div_par_exces(unsigned int n, unsigned int d)
{
    return (n + d - 1) / d; // On ajoute d - 1 pour arrondir à l'entier supérieur
}

unsigned int div_par_defaut(unsigned int n, unsigned int d)
{
    return n / d; // On utilise l'opérateur / car par défaut, il arrondit par default. (Logique hein ?)
}

unsigned int n_triplet_p(unsigned int n)
{
    if (n < 6)
        return 0; // On ne peut pas avoir de triplet pythagoricien avec un n < 6

    unsigned int cnt = 0;

    // Maximum
    // 1 + 2 + x = n
    // x = n - 3
    // c <= n - 3
    unsigned int c_max = n - 3;

    // Minimum
    // (x - 2) + (x - 1) + x = n
    // x = (n / 3) + 1
    // c >= (n / 3) + 1
    // c >= div_par_exces(n, 3) + 1
    // c > div_par_exces(n, 3)
    unsigned int c_min = div_par_exces(n, 3);

    for (unsigned int c = c_max; c > c_min; c -= 1) // On a inversé l'ordre de parcours de c pour optimiser un peu
    {
        // Maximum
        // 1 + x + c = n
        // x = n - c - 1
        // b <= n - c - 1
        // b < n - c && b < c
        // b < min(n - c, c)
        unsigned int b_max = n - c;
        if (b_max > c)
            b_max = c;

        // Minimum
        // (x - 1) + x + c = n
        // x = (n - c + 1) / 2
        // b >= (n - c + 1) / 2
        // b >= div_par_exces(n - c + 1, 2)
        unsigned int b_min = div_par_exces(n - c + 1, 2);

        for (unsigned int b = b_min; b < b_max; b += 1)
        {
            unsigned int a = n - b - c; // On calcule a

            // Juse pour être sûr que les calculs sont bons (de toute façon, ça sera optimisé par le compilateur)
            assert(a != 0 && b != 0 && c != 0 && a < b && b < c); // D'apres les calculs précédents, ça devrait être vrai

            if (a * a + b * b == c * c) // On teste si c'est un triplet pythagoricien
                cnt += 1;               // On incrémente le compteur
        }
    }
    // Bref, on a fait un peu de maths pour optimiser le parcours des boucles

    return cnt;
}

// 5. / 6.
unsigned int test_n_triplet_p()
{
    unsigned int ns[5] = {0, 5, 10, 100, 1000};
    unsigned int ress[5] = {0, 0, 0, 0, 1};

    unsigned int cnt = 0;
    char *neg, *pas;
    unsigned int n, res;
    for (unsigned int inc = 0; inc < 5; inc += 1)
    {
        n = ns[inc];

        res = n_triplet_p(n);

        if (res == ress[inc])
            neg = pas = "";
        else
        {
            cnt += 1;
            neg = "n'";
            pas = " pas";
        }
        printf("La fonction n_triplet_p appelée avec le paramètre %u renvoie %u ce qui %sest%s le bon résultat.\n", n, res, neg, pas);
    }

    return cnt;
}

// 6.
int main()
{
    return test_operation() +
           test_pgcd() + test_n_chiffres() + test_triplet_p() + test_n_triplet_p();
}

// 7.
// On utilise un unsigned long long int (64 bits) car on dépasse la limite d'un int
unsigned long long int multiplication(unsigned long long int x, unsigned long long int f, unsigned long long int mod)
{
    // On utilise l'algorithme de multiplication rapide auquel on ajoute le modulo
    unsigned long long int res = 0;
    while (f > 0)
    {
        if ((f & 1) == 1)          // Si f est impair (f&1==f%2)
            res = (res + x) % mod; // Pas d'overflow car (mod-1)*2 <= 2^64-1
        f >>= 1;                   // f /= 2
        x = (2 * x) % mod;         // Pareil ici
    }

    return res;
}

unsigned long long int puissance(unsigned int x, unsigned int p, unsigned long long int mod)
{
    // On utilise l'algorithme d'exponentiation rapide auquel on ajoute le modulo
    unsigned long long int res = 1;
    unsigned long long int l = x;
    while (p > 0)
    {
        if ((p & 1) == 1)                      // Si p est impair (p&1==p%2)
            res = multiplication(res, l, mod); // On ne peut pas utiliser l'opérateur * car il y a possibilité d'overflow
        p >>= 1;                               // p /= 2
        l = multiplication(l, l, mod);         // Pareil ici
    }

    return res;
}

unsigned long long int somme(unsigned int n, unsigned long long int mod)
{
    unsigned long long int s = 0;
    for (unsigned int inc = 1; inc <= n; inc += 1)
        s = (s + puissance(inc, inc, mod)) % mod;

    return s;
}

unsigned long long int reponse(unsigned int n)
{
    return somme(n, 10000000000);
}
