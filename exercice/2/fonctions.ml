(* 1. *)
let moyenne u v w x y z =
  (float_of_int (u + v + w) /. 3., float_of_int (x + y + z) /. 3.)

(* 2. *)
let racine a b c =
  match (b ** 2.) -. (4. *. a *. c) (* Calcul du discriminant *) with
  (* Deux racines réelles distinctes *)
  | d when d > 0. -> 2
  (* Une racine réelle double *)
  | 0. -> 1
  (* Pas de racine réelle *)
  | _ -> 0

(* 3. *)
let rec nombre_pas = function
  (* Si on a encore des calories a bruler, on fait 1000 pas *)
  | cal when cal > 0. -> 1000 + nombre_pas (cal -. 36.)
  (* Sinon on s'arrete *)
  | _ -> 0

(* 4. *)
(* Ici utilisera une application partielle de delta *)
let delta u n = u (n + 1) - u n

(* 5. *)
(* () necessaire car pas deterministique *)
let de () = Random.int 6 + 1 (* Simulation d'un de *)
let piece = Random.bool (* Simulation d'une piece *)

(* Les joueurs *)
let peter n = n + de ()

let tony n =
  let r = de () + de () in
  n + if r mod 2 = 0 then (* Pair *)
        r else (* Impair *)
            -r

let steve n = n + if piece () then (* Pile *)
                    2 else (* Face *)
                        0

(* On suppose que lorsque l'on enlève ou ajoute plus d'anneau que possible, on arrondis (Sinon les joueurs ne jouerais pas) *)
let borne = function n when n < 0 -> 0 | n when n > 100 -> 100 | n -> n

(* Fonction qui verifie si l'on a gagné *)
let gagne = function 100 -> true | _ -> false

let rec tour_peter n =
  let n = borne (peter n) in
  if gagne n then "peter" else tour_tony n

and tour_tony n =
  let n = borne (tony n) in
  if gagne n then "tony" else tour_steve n

and tour_steve n =
  let n = borne (steve n) in
  if gagne n then "steve" else tour_peter n

(* Fonction qui simule une partie *)
let partie () = tour_peter 0 (* () necessaire car pas deterministique *)

(* 6. *)
let est_premier n =
  let r =
    (* Partie entière de la racine *)
    int_of_float (sqrt (float_of_int n))
  in
  let rec boucle = function
    | i when i > r -> true
    | i when n mod i = 0 -> false
    | i -> boucle (i + 1)
  in
  boucle 2

(* Liste les premier inferieurs ou egaux à n *)
let rec liste_premiers n =
  if n = 1 then []
  else
    let l = liste_premiers (n - 1) in
    if est_premier n then n :: l else l

(* Si un nombre est un nombre de Hamming de type n, alors, il peut s'écrire p_1^e_1 * p_2^e_2 * ... * p_m^e_m avec les p_i <= n
   On enumère donc tout les e_i et on regarde si le nombre <= max *)
let rec calcul nb max = (* nb <= max *)
  function
  (* Plus de facteur *)
  | [] -> 1
  (*  Encore des facteurs *)
  | p :: lst ->
      let rec boucle nb =
        if nb > max then (* On depasse *)
          0
        else (* On continue *)
          calcul nb max lst + boucle (nb * p)
      in
      boucle nb

(* On genere la liste des premier pour optimiser l'usage du CPU au detriment de la memoire *)
let hamming n max = calcul 1 max (liste_premiers n)
let reponse = hamming 100 1000000000
