type entier = Zero | Successeur of entier

(* 1. *)
let quatre = Successeur (Successeur (Successeur (Successeur Zero)))

(* 2. *)
let rec int_vers_entier = function
  | n when n < 0 -> failwith "Pas cool !"
  | 0 -> Zero
  | n -> Successeur (int_vers_entier (n - 1))

let rec entier_vers_int = function
  | Zero -> 0
  | Successeur n -> 1 + entier_vers_int n

(* 3. *)
let rec add x = function Zero -> x | Successeur y -> Successeur (add x y)

(* 4. *)
let rec mult x = function Zero -> Zero | Successeur y -> add (mult x y) x

let rec expo x = function
  | Zero -> Successeur Zero
  | Successeur y -> mult (expo x y) x

(* 5. *)
type ratio = { num : entier; den : entier }

(* 6. *)
let ( +/ ) x y =
  { num = add (mult x.num y.den) (mult y.num x.den); den = mult x.den y.den }

let ( */ ) x y = { num = mult x.num y.num; den = mult x.den y.den }

(* 7. *)
(* Produit en croix *)
let r_eq x y = mult x.num y.den = mult x.den y.num

(* 8. *)
(* Soustraction (retourne aussi None quand x < y) *)
let rec sub x = function
  | Zero -> Some x
  | Successeur y -> ( match x with Zero -> None | Successeur x -> sub x y)

(* Division *)
let rec div x = function
  | Zero -> None
  | y -> (
      match sub x y with
      | None -> None
      | Some Zero -> Some (Successeur Zero)
      | Some s -> (
          match div s y with None -> None | Some d -> Some (Successeur d)))

(* PGCD *)
let rec pgcd x y =
  match (sub x y, sub y x) with
  | Some _, Some _ -> x (* x = y *)
  | None, Some s -> pgcd x s
  | Some s, None -> pgcd y s
  | None, None -> failwith "Impossible !"

(* Simplification *)
let simp x =
  let d = pgcd x.num x.den in
  match (div x.num d, div x.den d) with
  | Some n, Some d -> { num = n; den = d }
  | _ -> failwith "Impossible !"
